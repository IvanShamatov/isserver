class AddDeviceIdToDevice < ActiveRecord::Migration
  def change
    add_column :devices, :device_id, :string
  end
end
