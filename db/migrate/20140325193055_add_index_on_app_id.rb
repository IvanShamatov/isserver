class AddIndexOnAppId < ActiveRecord::Migration
  def change
    add_index :magazines, :app_id
  end
end
