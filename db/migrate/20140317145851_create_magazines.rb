class CreateMagazines < ActiveRecord::Migration
  def change
    create_table :magazines do |t|
      t.string :name
      t.string :shared_secret
      t.string :certificate

      t.timestamps
    end
  end
end
