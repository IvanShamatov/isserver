class AddDefaultAdminUserRole < ActiveRecord::Migration
  def change
    AdminUser.first.update_attributes(role: :admin)
  end
end
