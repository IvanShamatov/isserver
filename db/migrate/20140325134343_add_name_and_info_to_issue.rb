class AddNameAndInfoToIssue < ActiveRecord::Migration
  def change
    add_column :issues, :name, :string
    add_column :issues, :info, :text
  end
end
