class CreateIssues < ActiveRecord::Migration
  def change
    create_table :issues do |t|
      t.integer :magazine_id
      t.string :cover
      t.string :file
      t.string :product_id

      t.timestamps
    end
  end
end
