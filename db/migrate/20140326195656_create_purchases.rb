class CreatePurchases < ActiveRecord::Migration
  def change
    create_table :purchases do |t|
      t.text :receipt
      t.integer :magazine_id
      t.integer :device_id
      t.integer :env
      t.integer :status

      t.timestamps
    end
  end
end
