class AddAppIdToMagazine < ActiveRecord::Migration
  def change
    add_column :magazines, :app_id, :string
  end
end
