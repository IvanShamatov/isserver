class AddExpiresDateToPurchases < ActiveRecord::Migration
  def change
    add_column :purchases, :expires_date, :datetime
  end
end
