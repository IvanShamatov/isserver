class AddKindToPurchases < ActiveRecord::Migration
  def change
    add_column :purchases, :kind, :string
  end
end
