class AddAdminUserIdToMagazine < ActiveRecord::Migration
  def change
    add_column :magazines, :admin_user_id, :integer
  end
end
