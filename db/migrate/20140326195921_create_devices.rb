class CreateDevices < ActiveRecord::Migration
  def change
    create_table :devices do |t|
      t.string :token
      t.integer :magazine_id

      t.timestamps
    end
  end
end
