ISServer::Application.routes.draw do

  devise_for :admin_users, ActiveAdmin::Devise.config

  ActiveAdmin.routes(self)
  
  root to: "admin/dashboard#index"

  get  ":magazine_id/issues" => "magazines#show", magazine_id: /[^\/]+/

  get  ":magazine_id/purchases/:device_id" => "purchases#show", magazine_id: /[^\/]+/

  post ":magazine_id/purchases/:device_id" => "purchases#create", magazine_id: /[^\/]+/

  post ":magazine_id/tokens/:device_id" => "devices#create", magazine_id: /[^\/]+/

end
