# config valid only for Capistrano 3.1
lock '3.1.0'

set :application, 'example.com'
set :repo_url, 'REPOSITORY'


# Default value for linked_dirs is []
set :linked_dirs, %w{bin log tmp/pids tmp/cache tmp/sockets public/system}

set :shared_children, %w{public/uploads}

set :rvm_ruby_version, '2.1.1'

# Default value for default_env is {}
set :default_env, { rvm_bin_path: '~/.rvm/bin' }

# Default value for keep_releases is 5
set :keep_releases, 5

namespace :deploy do

  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      within release_path do
        execute :bundle, "exec thin restart -C config/thin/#{fetch(:stage)}.yml"
      end
    end
  end
  before :restart, 'rvm:hook'

  task :start do
    on roles(:app), in: :sequence, wait: 5 do
      within release_path do
        execute :bundle, "exec thin start -C config/thin/#{fetch(:stage)}.yml"
      end
    end
  end
  before :start, 'rvm:hook'

  after :publishing, :restart

  after :restart, :clear_cache do
    on roles(:web), in: :groups, limit: 3, wait: 10 do
      # Here we can do anything such as:
      # within release_path do
      #   execute :rake, 'cache:clear'
      # end
    end
  end

end