module MagazinesHelper

  def full_url_for(url)
    request.protocol + request.host_with_port + url
  end
end
