class DevicesController < ApplicationController
  
  def create
    m = Magazine.find_by_app_id(params[:magazine_id])
    Device.create(magazine_id: m.id, token: params[:apns_token], device_id: params[:device_id])
    render nothing:true, status: 200
  end
end
