class MagazinesController < ApplicationController
  respond_to :json, :xml

  def show
    @magazine = Magazine.find_by_app_id(params[:magazine_id])
                        
    respond_with @magazine
  end
end
