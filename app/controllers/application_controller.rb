class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :null_session

  # rescue_from ActiveRecord::RecordNotFound do 
  #   render text: []
  # end

  def redirect_user_to_home(exception)
    redirect_to root_path, :alert => exception.message and return
  end

end
