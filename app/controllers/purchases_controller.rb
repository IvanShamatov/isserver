class PurchasesController < ApplicationController
  respond_to :json

  def show
    m = Magazine.find_by(app_id: params[:magazine_id])
    d = Device.find_by(device_id: params[:device_id], magazine_id:m.id)

    if m.nil? || d.nil?
      @purchases = []
      @subscribed = false
    else
      @subscribed = !!Purchase.where(magazine_id: m.id, device_id: d.id, kind: Purchase::SUBSCRIPTION, status: Purchase::STATUS_VERIFIED)
      if m && d
        @purchases = Issue.where(magazine_id: m.id).pluck(:product_id).select {|a| !a.empty?}
      else
        @purchases = Purchase.where(magazine_id: m.id, device_id: d.id, kind: Purchase::ISSUE, status: Purchase::STATUS_VERIFIED).pluck(:product_id)
      end
    end
    respond_with [@purchases, @subscribed]
  end

  def create
    m = Magazine.find_by(app_id: params[:magazine_id])
    d = Device.find_or_create_by(device_id: params[:device_id], magazine_id:m.id)   
    kind = params[:type] 
    Purchase.create(device_id: d.id, magazine_id: m.id,  receipt: params[:receipt_data], kind: kind)
    render nothing:true, status: 200
  end
end
