json.array! @magazine.issues.each do |issue|
  json.name issue.name
  json.title issue.name
  json.info issue.info
  json.date issue.published_at.strftime("%Y-%m-%d %H:%M:%S")
  json.cover full_url_for(issue.cover.main.url)
  json.url full_url_for(issue.file.url)
  json.product_id issue.product_id unless issue.product_id.empty?
end