xml.feed xmlns:"http://www.w3.org/2005/Atom", :"xmlns:news" => "http://itunes.apple.com/2011/Newsstand" do
  xml.updated @magazine.issues.last.updated_at.strftime('%Y-%m-%dT%R:%SZ')
  @magazine.issues.order(updated_at: :desc).each do |issue|
    xml.entry do 
      xml.id issue.id
      xml.updated issue.updated_at.strftime('%Y-%m-%dT%R:%SZ')
      xml.published issue.published_at.strftime('%Y-%m-%dT%R:%SZ')
      xml.summary issue.info
      xml.tag!("news:cover_art_icons") do 
        xml.tag!("news:cover_art_icon", size: "SOURCE", src: full_url_for(issue.cover.atom.url))
      end
    end
  end
end