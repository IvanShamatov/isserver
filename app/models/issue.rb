class Issue < ActiveRecord::Base

  belongs_to :magazine
  mount_uploader :file, PdfUploader
  mount_uploader :cover, CoverUploader

  validates :cover, :file, presence: true
  validates :name, uniqueness: true
  validates :product_id, uniqueness: true, :if => Proc.new {!product_id.empty?}

  before_save :check_field

  after_create :push_notifications

  def check_field
    if published_at.nil?
      self.published_at = DateTime.now
    end
  end

  def push_notifications
    devs = self.magazine.devices.pluck(:token)
    devs.each do |token|
      notification = Grocer::NewsstandNotification.new(device_token: token)
      pusher.push notification
    end
  end

  def pusher
    @pusher ||= Grocer.pusher(certificate: self.magazine.certificate.path)
  end



end