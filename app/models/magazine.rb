class Magazine < ActiveRecord::Base

  has_many :issues
  has_many :purchases
  has_many :devices
  belongs_to :admin_user

  mount_uploader :certificate, CertificateUploader

  default_scope { includes(:issues).references(:issues)} 

  validates :certificate, :shared_secret, :app_id, presence: true

  validates :app_id, uniqueness: true
end
