class Device < ActiveRecord::Base
  has_many :purchases
  has_many :magazines
end
