class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= AdminUser.new # guest user (not logged in)

    if user.admin?
      can :manage, :all
    end

    if user.client?
      can :manage, [Magazine, Issue]
    end
    
    can :read, ActiveAdmin::Page, :name => "Dashboard"

  end
end
