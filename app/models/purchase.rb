require 'itunes/receipt'

class Purchase < ActiveRecord::Base
  belongs_to :device
  belongs_to :magazine

  STATUS_INPROGRESS = 0
  STATUS_VERIFIED = 1
  STATUS_FAILED = 2
  STATUS_EXPIRED = 3

  ISSUE = "issue"
  SUBSCRIPTION = "auto-renewable-subscription"

  after_create :validate_in_appstore


  def validate_in_appstore
    if self.kind == ISSUE
      receipt = Itunes::Receipt.verify! self.receipt, :allow_sandbox_receipt
      self.status = STATUS_VERIFIED
      self.product_id = receipt.product_id
      self.save
    else
      secret = self.magazine.shared_secret
      Itunes.shared_secret = secret
      receipt = Itunes::Receipt.verify! self.receipt, :allow_sandbox_receipt
      self.status = STATUS_VERIFIED
      self.expires_date = receipt.expires_date
      self.save
    end
  end


  def self.check_expiration
    Itunes.shared_secret = self.magazine.shared_secret
    subs = Purchase.where(status: STATUS_VERIFIED, kind: SUBSCRIPTION)
    subs.each do |s|
      begin
        Itunes.shared_secret = s.magazine.shared_secret
        receipt = Itunes::Receipt.verify! s.receipt, :allow_sandbox_receipt
        s.expires_date = receipt.expires_date
        s.status = STATUS_VERIFIED
      rescue Itunes::Receipt::VerificationFailed => e
        s.status = STATUS_FAILED
      rescue Itunes::Receipt::ExpiredReceiptReceived => e
        s.status = STATUS_EXPIRED
      ensure
        s.save
      end
    end
  end
end
