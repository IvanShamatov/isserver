# encoding: utf-8

class CoverUploader < CarrierWave::Uploader::Base

  include CarrierWave::MiniMagick

  storage :file
  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end
  version :main do 
    process :resize_to_fit => [300, 300]
  end

  version :atom do 
    process :resize_to_fit => [1024, 1024]
  end

  def extension_white_list
    %w(jpg jpeg png)
  end

end
