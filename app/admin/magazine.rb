ActiveAdmin.register Magazine do


  index do 
    column :name
    column :issues do |m|
      link_to "Take a look", admin_magazine_issues_path(m)
    end
    actions
  end

  form do |f|
    f.inputs do 
      f.input :name
      f.input :app_id
      f.input :shared_secret
      f.input :certificate
    end
    f.actions
  end
  
  permit_params :app_id, :name, :certificate, :shared_secret
  

  controller do 
    before_filter :check_user, only: [:show]

    def check_user
      if current_admin_user.client?
        unless current_admin_user.magazines.include?(Magazine.find(params[:id]))
          redirect_to :root_path
        end
      end
    end

    def scoped_collection
      if current_admin_user.client?
        Magazine.where(admin_user_id: current_admin_user.id)
      else
        Magazine.all
      end
    end

    def create
      @magazine = Magazine.new(permitted_params[:magazine])
      @magazine.admin_user_id = current_admin_user.id
      super
    end
  end

end
