ActiveAdmin.register Issue do

  belongs_to :magazine

  action_item only: :edit do
    link_to "Delete Magazine", admin_magazine_issue_path(issue.magazine, issue), {method: :delete}
  end


  index as: :grid do |issue|
    html = link_to image_tag(issue.cover.url, height: 150), edit_admin_magazine_issue_path(issue.magazine, issue)
    html += "<br>".html_safe
    html += link_to(issue.name, edit_admin_magazine_issue_path(issue.magazine, issue))
    html
  end

  form do |f|
    f.inputs do 
      f.input :name
      f.input :info
      f.input :cover
      f.input :file
      f.input :product_id, placeholder: "Leave empty, if issue want issue to be free"
      f.input :published_at, as: :datepicker, default: DateTime.now
    end
    f.actions
  end


  permit_params :magazine_id, :file, :product_id, :cover, :name, :info

  controller do 
    before_filter :check_user

    def check_user
      if current_admin_user.client?
        unless current_admin_user.magazines.include?(Magazine.find(params[:magazine_id]))
          redirect_to :root, error: "You don't have access to this magazine"
        end
      end
    end
  end

end
