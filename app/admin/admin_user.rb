ActiveAdmin.register AdminUser do

  menu label: "Users", :if => proc{ current_admin_user.admin? }
  permit_params :email, :password, :password_confirmation, :role

  index do
    id_column
    column :email
    column :current_sign_in_at
    column :sign_in_count
    column :created_at
    actions
  end
  
  form do |f|
    f.inputs "Admin Details" do
      f.input :email
      f.input :password
      f.input :password_confirmation
      f.input :role, as: :select, collection: AdminUser.roles
    end
    f.actions
  end

end
